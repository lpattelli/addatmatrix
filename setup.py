import os
import subprocess
import shutil
from setuptools.command.install import install
from setuptools.command.egg_info import egg_info
from setuptools import setup, find_packages

_DEVICE = os.environ.get("ADDA_DEVICE", "cpu")

class CustomInstallCommand(install):
    def run(self):
        if not os.path.exists("adda"):
            download_adda()
            compile_c_code()
        super().run()

class CustomDevelopCommand(install):
    def run(self):
        if not os.path.exists("adda"):
            download_adda()
            compile_c_code()
        super().run()

class CustomEggInfoCommand(egg_info):
    def run(self):
        # Call your function to download and compile the C code here
        if not os.path.exists("adda"):
            download_adda()
            compile_c_code()
        super().run()        

def download_adda():
    download_command = 'git clone https://github.com/adda-team/adda && cd adda'
    subprocess.run(download_command, shell=True, check=True)

def compile_c_code():
    cwd = os.getcwd()
    adda_dir = os.path.join("adda","src")
    if _DEVICE == 'cpu':
        mode, binary = ('seq','adda')
    elif _DEVICE == 'gpu':
        mode, binary = ('ocl','adda_ocl')
    os.chdir(adda_dir)
    compile_command = f'make {mode}'
    subprocess.run(compile_command.split(' '), check=True)
    os.chdir(cwd)
    file_ext = "exe" if os.name=="nt" else "bin"
    if os.name=="nt":
        binary = binary + '.exe'
    shutil.copy(os.path.join(adda_dir,mode,binary),os.path.join('addatmatrix',f'adda.{file_ext}'))

setup(
    name='addatmatrix',
    version='0.1.0',
    packages=find_packages(),
    cmdclass={
        'install': CustomInstallCommand,
        'develop': CustomDevelopCommand,
        'egg_info': CustomEggInfoCommand,
    },
    package_data={
        'addatmatrix': ['adda.*'],
    },
    author='Krzysztof Czajkowski',
    author_email='kmczajkowski@int.pl',
    description='A package for calculating T matrices with ADDA',
    url='https://github.com/k.czajkowski/addatmatrix',
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
)


