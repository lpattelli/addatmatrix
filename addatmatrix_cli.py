#!/usr/bin/env python3
import addatmatrix.t_matrix
import addatmatrix.cross_section
import addatmatrix.data
import addatmatrix.wrapper
import click

@click.group()
def cli():
    """A simple CLI for addatmatrix package"""
    pass

@click.command(help="Computes T-matrix from multiple runs of ADDA")
@click.option("--adda_string", default="",help="String defining simulation parameters for ADDA,\
              must not contain: -prognosis, -save_geom, -scat_matr, -store_dip_pol, and -surf")
@click.option("--max_order", default=3, help="Maximal multipole order (l_max in Smuthi convention).")
@click.option('--num_theta', default=91, help='Number of polar angles for the scattered field integration grid.')
@click.option('--num_phi', default=181, help='Number of azimuthal angles for the scattered field integration grid.')
@click.option('--num_illuminations', default=20, help='Number of plane-wave illuminations per polarization.')
@click.option('--particle_z',default=0., help='Particle center position z-coordinate relative to the origin\
              center (only for debugging)')
@click.option('--output_path',default="tmatrix.h5", help='Path of the output T-matrix h5 file')
@click.option('--wavelength',default=6.283185307179586, help="Wavelength for metadata purposes \
              (if -lambda is not given in adda_string)")
def tmatrix(adda_string, max_order, num_theta, num_phi, num_illuminations, particle_z, output_path, wavelength):
    tmatrix_data, metadata, indices = addatmatrix.t_matrix.get_t_matrix(adda_string, max_order,
                                                                        num_theta, num_phi, num_illuminations,
                                                                        particle_z, wavelength)
    addatmatrix.data.write_t_matrix_data(output_path, tmatrix_data, indices, metadata)

@click.command(help="Computes cross sections for various wavelengths")
@click.argument('min_wavelength',type=click.FLOAT)
@click.argument('max_wavelength',type=click.FLOAT)
@click.argument('num_wavelength',type=click.INT)
@click.option('--adda_string', default="",
              help="String defining simulation parameters for ADDA, must not contain: -scat_matr and -lambda")
@click.option('--output_path',default="cs_spect.npz", help="Path to the output npz file")
def spectrum(min_wavelength, max_wavelength, num_wavelength, adda_string, output_path):
    csx, csy = addatmatrix.cross_section.get_cross_section(min_wavelength, max_wavelength, num_wavelength, adda_string)
    addatmatrix.data.write_cross_section(csx, csy, output_path)

cli.add_command(tmatrix)
cli.add_command(spectrum)

if __name__ == '__main__':
    cli()
