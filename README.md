# addatmatrix

**addatmatrix** is a Python package that provides a convenient interface for calculating the T-matrix using the ADDA code.

## Installation
Set your target device via environment variable `ADDA_DEVICE` to `cpu` or `gpu`:
```commandline
export ADDA_DEVICE=gpu
```

Install **addatmatrix** using pip:

```bash
pip install -r requirements.txt
pip install .
```
## Usage
### CLI
The most straight forward way of using `addatmatrix` is via the CLI:

`./addatmatrix_cli.py COMMAND [ARGS]`

Commands:
- `tmatrix` - Computes T-matrix from multiple runs of ADDA
- `spectrum` -  Computes cross sections for various wavelengths

A minimal example:
```commandline
./addatmatrix_cli.py tmatrix --adda_string "-m 2.5 0.0"
```
where `adda_string` argument is a string provided to ADDA CLI

Output format:
- The T-matrix is stored as an hdf5, following conventions decribed in Ref. 1.
- The spectrum is stored as a npy file.

Help on specific command:
```commandline
./addatmatrix_cli.py tmatrix --help
```


### Scripting
The `tmatrix` command is a wrapper for `addatmatrix.t_matrix.get_t_matrix` function. 

A usage example has been shown in `examples/sphere_dscs.py`, which can be run like this:
```commandline
python examples/sphere_dscs.py
```
from the main repository directory.

## References
If you use this code in your work, please cite the following papers:
- Asadova N. et al. T-matrix representation of optical scattering response: Suggestion for a data format, J. Quant. Spectrosc. Radiat. Transfer 333, 109310 (2025)
- Yurkin M.A. and Hoekstra A.G. The discrete-dipole-approximation code ADDA: capabilities and known limitations, J. Quant. Spectrosc. Radiat. Transfer 112, 2234–2247 (2011)