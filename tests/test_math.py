import numpy as np
import pytest

import addatmatrix.math as math

@pytest.fixture
def args_1():
    theta = np.pi/4*3
    theta = np.array([theta])
    return (3,1,theta)

@pytest.fixture
def reference_plm_1():
    return 0.85923294

@pytest.fixture
def reference_pilm_1():
    return 1.21513888

@pytest.fixture
def reference_taulm_1():
    return 2.00487687

@pytest.fixture
def args_legendre():
    theta = np.pi/4*3
    theta = np.array([theta])
    return (3,1,theta)

@pytest.fixture
def reference_legendre():
    return np.array([0.85923294, 1.21513888, 2.00487687])

@pytest.fixture
def args_spherical_n():
    return 1

@pytest.fixture
def args_spherical_m():
    return 1

@pytest.fixture
def reference_spherical_n():
    return 1

@pytest.fixture
def reference_spherical_m():
    return 1

@pytest.fixture
def args_coefficients():
    l = 2
    m= -1
    tau = 0
    pol = 1
    theta = np.array([np.pi/4])
    k = 0.7
    kp = k * np.sin(theta)
    kz = k * np.cos(theta)
    dagger = True
    return tau, l, m, pol, kp, kz, dagger

@pytest.fixture
def reference_coefficients():
    ref = np.array([-0.-0.39528471j])
    return ref

@pytest.fixture
def args_sph_n():
    theta = np.pi/3*1.2
    phi = np.pi/5
    l = 3
    m = 2
    theta = np.array([theta])
    return l, m, theta, phi

@pytest.fixture
def reference_sph_n():
    return np.array([[-0.10965469-0.33748244j],
       [-0.29231698+0.09497955j]])

@pytest.fixture
def args_sph_m():
    theta = np.pi/3*1.2
    theta = np.array([theta])
    phi = np.pi/5
    l = 3
    m = 2    
    return l, m, theta, phi

@pytest.fixture
def reference_sph_m():
    return np.array([[-0.29231698+0.09497955j],
       [ 0.10965469+0.33748244j]])

@pytest.mark.parametrize('args, reference', [
    ('args_1','reference_plm_1')])
def test_plm(args, reference, request):
    l, m, x = request.getfixturevalue(args)
    val = math.plm(l, m, x)
    assert np.allclose(val, request.getfixturevalue(reference))

@pytest.mark.parametrize('args, reference', [
    ('args_1','reference_pilm_1')])
def test_pilm(args, reference, request):
    l, m, x = request.getfixturevalue(args)
    val = math.pilm(l, m, x)
    assert np.allclose(val, request.getfixturevalue(reference))

@pytest.mark.parametrize('args, reference', [
    ('args_1','reference_taulm_1')])
def test_taulm(args, reference, request):
    l, m, x = request.getfixturevalue(args)
    val = math.taulm(l, m, x)
    assert np.allclose(val, request.getfixturevalue(reference))

@pytest.mark.parametrize('args, reference', [
    ('args_legendre','reference_legendre')])
def test_legendre_normalized(args, reference, request):
    l, m, x = request.getfixturevalue(args)
    val = math.legendre_normalized(l, m, x)
    val = (np.array(val).T)[0]
    assert np.allclose(val, request.getfixturevalue(reference))

@pytest.mark.parametrize('args, reference', [
    ('args_sph_m','reference_sph_m')
])
def test_spherical_harmonic_m(args, reference, request):
    l, m, theta, phi = request.getfixturevalue(args)
    val = math.spherical_harmonic_m(theta, phi, l ,m)
    assert np.allclose(val, request.getfixturevalue(reference))

@pytest.mark.parametrize('args, reference', [
    ('args_sph_n','reference_sph_n')
])
def test_spherical_harmonic_n(args, reference, request):
    l, m, theta, phi = request.getfixturevalue(args)
    val = math.spherical_harmonic_n(theta, phi, l, m)
    assert np.allclose(val, request.getfixturevalue(reference))

@pytest.mark.parametrize('args, reference', [
    ('args_coefficients','reference_coefficients')])
def test_transformation_coefficients_vwf(args, reference, request):
    tau, l, m, pol, kp, kz, dagger = request.getfixturevalue(args)
    val = math.transformation_coefficients_vwf(tau, l, m, pol, kp, kz, dagger)
    assert np.allclose(val, request.getfixturevalue(reference))


@pytest.fixture
def args_pwe():
    z = 1
    rad = 2
    c = 2
    n = 1
    lmax = 3
    lam = 2 * np.pi
    kth = np.array([np.pi/1.2])
    azimuthal_angle = np.array([np.pi/4])[:,None]
    pol = 1

    return lmax, [0,0,c], [0,0,c], np.sin(kth), np.cos(kth),azimuthal_angle, pol

@pytest.fixture
def reference_pwe():
    ref = np.array([-1.22474487e+00-1.22474487e+00j,  0.00000000e+00+0.00000000e+00j,
            1.22474487e+00-1.22474487e+00j,  1.11803399e+00+8.00258966e-17j,
        -1.36930639e+00+1.36930639e+00j,  0.00000000e+00+0.00000000e+00j,
        -1.36930639e+00-1.36930639e+00j,  1.11803399e+00+2.16945571e-16j,
        -4.52855523e-01+4.52855523e-01j,  1.29656771e-16-1.81142209e+00j,
            1.28619473e+00+1.28619473e+00j,  0.00000000e+00+0.00000000e+00j,
        -1.28619473e+00+1.28619473e+00j,  3.51491998e-16-1.81142209e+00j,
            4.52855523e-01+4.52855523e-01j, -1.06066017e+00-1.06066017e+00j,
        -1.22474487e+00-1.62657972e-16j, -1.06066017e+00+1.06066017e+00j,
            9.68245837e-01+6.93044594e-17j, -7.90569415e-01+7.90569415e-01j,
        -3.14985808e-16+2.37170825e+00j,  7.90569415e-01+7.90569415e-01j,
        -9.68245837e-01-1.87880376e-16j, -3.92184387e-01+3.92184387e-01j,
            9.35717147e-17-1.30728129e+00j,  1.01261573e-01+1.01261573e-01j,
            3.15052079e+00+4.18419651e-16j,  1.01261573e-01-1.01261573e-01j,
        -2.53667500e-16+1.30728129e+00j, -3.92184387e-01-3.92184387e-01j])
    return ref


@pytest.mark.parametrize('args, reference', [
    ('args_pwe','reference_pwe')])
def test_pwe_to_swe_conversion(args, reference, request):
    (
        l_max, reference_point,
        pwe_reference_point, k_parallel, k_z,
        azimuthal_angle, pol
    ) = request.getfixturevalue(args)
    val = math.pwe_to_swe_conversion(l_max, l_max, reference_point,
                                     pwe_reference_point, pol, k_parallel,
                                     k_z, azimuthal_angle)
    assert np.allclose(val, request.getfixturevalue(reference), rtol=1e-3)
