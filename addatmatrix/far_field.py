from typing import Tuple

import numpy as np
from numba import njit

from addatmatrix.spatial import IntegrationGrid
from addatmatrix.math import spherical_harmonic_m, spherical_harmonic_n

@njit
def getposfac(ks, er, pos):
    er_dot_r = er[0] * pos[0] + er[1] * pos[1] + er[2] * pos[2]
    return np.exp(-1j * ks * er_dot_r)

def parse_pol_file(data: np.array) -> Tuple[np.array, np.array]:
    r = data[:, :3]
    pol = np.zeros((data.shape[0], 3), dtype=complex)
    pol[:, 0] = data[:, 4] + 1j * data[:, 5]
    pol[:, 1] = data[:, 6] + 1j * data[:, 7]
    pol[:, 2] = data[:, 8] + 1j * data[:, 9]
    return pol, r

@njit
def get_total_polarization(pol: np.array, er: np.array, r: np.array, ks: float) -> np.array:
    pol_eff = np.zeros_like(er) + 1j * np.zeros_like(er)
    for ip, pos in enumerate(r):
        posfac = getposfac(ks, er, pos)
        pol_eff[0,:,:] = pol_eff[0,:,:] + pol[ip, 0] * posfac
        pol_eff[1,:,:] = pol_eff[1,:,:] + pol[ip, 1] * posfac
        pol_eff[2,:,:] = pol_eff[2,:,:] + pol[ip, 2] * posfac 
    return pol_eff

def convert_cartesian_to_spherical(pol_eff: np.array, grid: IntegrationGrid) -> np.array:
    T, P = (grid.T, grid.P)
    pol_sph = np.zeros((2, pol_eff.shape[1], pol_eff.shape[2]), dtype=complex)
    pol_sph[0,:,:] = np.cos(T)*np.cos(P)*pol_eff[0,:,:]+np.cos(T)*np.sin(P)*pol_eff[1,:,:]-np.sin(T)*pol_eff[2,:,:]
    pol_sph[1,:,:] = -np.sin(P)*pol_eff[0,:,:]+np.cos(P)*pol_eff[1,:,:]
    return pol_sph

def calculate_far_field(pol: np.array, ks: float) -> np.array:
    Escat = np.zeros(pol.shape, dtype=complex)
    Escat[1, :, :] = 1j * ks ** 3 * pol[0, :, :]
    Escat[0, :, :] = 1j * ks ** 3 * pol[1, :, :]
    Escat[1, :, :] = -Escat[1, :, :]
    Escat = np.nan_to_num(Escat)
    return Escat

def azimuthal_average_far_field(b_coeffs, theta, phi, k, lmax):
    res = []
    phi_shape = phi.shape
    azimuthal_angles = phi[0]
    phi = phi.flatten()
    theta = theta.flatten()
    for tau in [0, 1]:
        for l in range(1, lmax + 1):
            for m in range(-l, l + 1):
                if tau:
                    mr = spherical_harmonic_m(theta, phi, l, m)
                else:
                    mr = 1j * spherical_harmonic_n(theta, phi, l, m)
                prefactor = (-1j) ** (l + 1)
                res.append(prefactor * mr)
    res = np.array(res)
    myff = (b_coeffs[:, None, None] * res).sum(axis=0)

    myff = myff.reshape(-1, *phi_shape)
    myff[1] = -myff[1]

    azimuthal_average = np.trapz(np.abs(myff) ** 2 / (k ** 2), azimuthal_angles)
    return azimuthal_average