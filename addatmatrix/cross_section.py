import addatmatrix.wrapper
import sys
import addatmatrix.far_field
import subprocess
import numpy as np
import tqdm
from tqdm import trange

def get_cross_section(min_wavelength, max_wavelength, num_wavelength, adda_string):
    wavelength_range = np.linspace(min_wavelength, max_wavelength, num_wavelength)

    csx = []
    csy = []
    for wl in tqdm.tqdm(wavelength_range):
        adda_output = addatmatrix.wrapper.run_from_args(adda_string + f" -scat_matr none -lambda {wl}")
        csx.append(adda_output.cross_section_x)
        csy.append(adda_output.cross_section_y)

    csx = np.array(csx)
    csy = np.array(csy)
    return csx, csy

def calculate_orientation_averaged_extinction_cross_section(adda_callable, angle_grid, **kwargs):
    cext_tot = []
    for iang in trange(len(angle_grid)):
        # TODO: PWE objects
        pol, kth, kph = angle_grid[iang]

        adda_output = adda_callable(**kwargs, kth=kth, kph=kph, Nphi=Nphi, Ntheta=Ntheta)

        data = adda_output.dipole_polarization_x if pol == 1 else adda_output.dipole_polarization_y
        cext = adda_output.cross_section_x if pol == 1 else adda_output.cross_section_y
        cext = cext[0]
        cext_tot.append(cext)
    return np.mean(cext_tot)

if __name__ == "__main__":
    min_wavelength = float(sys.argv[1])
    max_wavelength = float(sys.argv[2])
    num_wavelength = int(sys.argv[3])
    adda_string = sys.argv[4]

    csx, csy = get_cross_section(min_wavelength, max_wavelength, num_wavelength, adda_string)

    np.save("csx",csx)
    np.save("csy",csy)

