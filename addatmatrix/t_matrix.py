import numpy as np
from tqdm import trange
from scipy.linalg import pinv

import addatmatrix.wrapper
import addatmatrix.data
from addatmatrix import far_field
from addatmatrix.wrapper import ellipsoid_pol, geom_from_file, arbitrary_geom_pol, arbitrary_geom_pol_aniso, pol_from_adda_string
from addatmatrix.math import double_trapezoid_integral, pwe_to_swe_conversion, spherical_harmonic_m, spherical_harmonic_n
from addatmatrix.spatial import IntegrationGrid, FibonacciIlluminationGrid, UniformIntegrationGrid
from addatmatrix.wrapper import USE_SURF
from addatmatrix.data import t_matrix_to_smuthi_convention

_EPS = 1e-2
ILLUMINATION_GRID = FibonacciIlluminationGrid

def solve_t_matrix(scat_tot: np.array, inc_tot: np.array) -> np.array:
    scat_tot = np.array(scat_tot).T
    inc_tot = np.array(inc_tot).T
    inc_tot_inv = pinv(inc_tot)
    T = scat_tot @ inc_tot_inv
    return T

def calculate_scattered_field_coefficients(Escat: np.array, ks: float,
                                           lmax: int, grid: IntegrationGrid) -> np.array:
    scatf1 = np.zeros(2 * lmax * (lmax + 2), dtype=complex)
    ii = 0
    for tau in [0, 1]:
        for l in range(1, lmax + 1):
            for m in range(-l, l + 1):
                if tau:
                    mr = spherical_harmonic_m(grid.T, grid.P, l, m)
                else:
                    mr = 1j * spherical_harmonic_n(grid.T, grid.P, l, m)
                prefactor = 1 / (-1j) ** (l + 1)
                int1 = double_trapezoid_integral(mr.conj() * Escat * prefactor, grid)
                int2 = double_trapezoid_integral(mr.conj() * mr, grid) # TODO: use normalization
                scatf1[ii] = int1 / int2
                ii += 1
    return scatf1

def t_matrix_spheroid(rad=1.,ar=1.5,n=2,lmax=3,nsub=1.00001+0j,
                      Nil=20,Nphi=181,Ntheta=91,Ngrid=20,ddaeps=2.5,
                      ks = 1, seps = 1e-2):
    z = rad * ar

    grid = UniformIntegrationGrid(Nphi, Ntheta)
    illumination_grid = ILLUMINATION_GRID(illumination_number=Nil)
    angle_grid = illumination_grid.get_angle_grid()

    scat_tot = np.zeros((2 * Nil, 2 * lmax * (lmax + 2)), dtype=complex)
    inc_tot = np.zeros((2 * Nil, 2 * lmax * (lmax + 2)), dtype=complex)
    
    for iang in trange(2 * Nil):
        # TODO: PWE objects
        pol, kth, kph = angle_grid[iang]


        ellipsoid_pol(rad, n, yr=1, zr=ar, grid=Ngrid,
                      eps=ddaeps, kth=kth, kph=kph, ms=nsub)

        data = far_field.read_pol_file(pol)
        pol_cart, r = far_field.parse_pol_file(data)
        pol_eff = far_field.get_total_polarization(pol_cart, grid.er, r, ks)
        pol_eff_sph = far_field.convert_cartesian_to_spherical(pol_eff, grid)
        Escat = far_field.calculate_far_field(pol_eff_sph, ks)
        pos = [0, 0, z]
        kth_arr = np.array([kth])
        azimuthal_angle = np.array([kph])[:,None]
        incf = pwe_to_swe_conversion(lmax, lmax, pos, pos, int(pol), np.sin(kth_arr), 
                                     np.cos(kth_arr), azimuthal_angle)
        
        scatf1=calculate_scattered_field_coefficients(Escat, ks, lmax, grid)
        scat_tot[iang, :] = scatf1
        inc_tot[iang, :] = incf

    T = solve_t_matrix(scat_tot, inc_tot)
    return T

def t_matrix_arbitrary_geom(rad=1.,ar=1.5,n=2,lmax=3,nsub=1.00001+0j,
                      Nil=20,Nphi=181,Ntheta=91,Ngrid=20,ddaeps=2.5,
                      ks = 1, seps = 1e-2):
    z = rad * ar

    grid = UniformIntegrationGrid(Nphi, Ntheta)
    illumination_grid = ILLUMINATION_GRID(illumination_number=Nil)
    angle_grid = illumination_grid.get_angle_grid()

    scat_tot = np.zeros((2 * Nil, 2 * lmax * (lmax + 2)), dtype=complex)
    inc_tot = np.zeros((2 * Nil, 2 * lmax * (lmax + 2)), dtype=complex)
    
    for iang in trange(2 * Nil):
        # TODO: PWE objects
        pol, kth, kph = angle_grid[iang]

        adda_output = geom_from_file(rad, n, yr=1, zr=ar, grid=Ngrid,
                      eps=ddaeps, kth=kth, kph=kph, ms=nsub)

        data = adda_output.dipole_polarization_x if pol == 1 else adda_output.dipole_polarization_y
        pol_cart, r = far_field.parse_pol_file(data)
        pol_eff = far_field.get_total_polarization(pol_cart, grid.er, r, ks)
        pol_eff_sph = far_field.convert_cartesian_to_spherical(pol_eff, grid)
        Escat = far_field.calculate_far_field(pol_eff_sph, ks)
        pos = [0, 0, z]
        kth_arr = np.array([kth])
        azimuthal_angle = np.array([kph])[:,None]
        incf = pwe_to_swe_conversion(lmax, lmax, pos, pos, int(pol), np.sin(kth_arr), 
                                     np.cos(kth_arr), azimuthal_angle)
        
        scatf1 = calculate_scattered_field_coefficients(Escat, ks, lmax, grid)
        scat_tot[iang, :] = scatf1
        inc_tot[iang, :] = incf

    T = solve_t_matrix(scat_tot, inc_tot)
    return T

def t_matrix_arbitrary_geom_pol(
        shape, params, rad=1.,ar=1.5,n=2,lmax=3,nsub=1.00001+0j,
        Nil=20,Nphi=181,Ntheta=91,Ngrid=20,ddaeps=2.5,
        ks = 1, seps = 1e-2):
    z = rad * ar

    grid = UniformIntegrationGrid(Nphi, Ntheta)
    illumination_grid = ILLUMINATION_GRID(illumination_number=Nil)
    angle_grid = illumination_grid.get_angle_grid()

    scat_tot = np.zeros((2 * Nil, 2 * lmax * (lmax + 2)), dtype=complex)
    inc_tot = np.zeros((2 * Nil, 2 * lmax * (lmax + 2)), dtype=complex)
    
    for iang in trange(2 * Nil):
        # TODO: PWE objects
        pol, kth, kph = angle_grid[iang]

        adda_output = arbitrary_geom_pol(shape, params, n, rad, ar, 
                                         Ngrid, ddaeps, Ntheta, Nphi,
                                         kth, kph, nsub)

        data = adda_output.dipole_polarization_x if pol == 1 else adda_output.dipole_polarization_y
        pol_cart, r = far_field.parse_pol_file(data)
        pol_eff = far_field.get_total_polarization(pol_cart, grid.er, r, ks)
        pol_eff_sph = far_field.convert_cartesian_to_spherical(pol_eff, grid)
        Escat = far_field.calculate_far_field(pol_eff_sph, ks)
        pos = [0, 0, z]
        kth_arr = np.array([kth])
        azimuthal_angle = np.array([kph])[:,None]
        incf = pwe_to_swe_conversion(lmax, lmax, pos, pos, int(pol), np.sin(kth_arr), 
                                     np.cos(kth_arr), azimuthal_angle)
        
        scatf1 = calculate_scattered_field_coefficients(Escat, ks, lmax, grid)
        scat_tot[iang, :] = scatf1
        inc_tot[iang, :] = incf

    T = solve_t_matrix(scat_tot, inc_tot)
    return T

def t_matrix_arbitrary_geom_pol_aniso(
        shape, params, n, rad=1.,ar=1.5,lmax=3,nsub=1.00001+0j,
        Nil=20,Nphi=181,Ntheta=91,Ngrid=20,ddaeps=2.5,
        ks = 1, seps = 1e-2):
    z = rad * ar

    grid = UniformIntegrationGrid(Nphi, Ntheta)
    illumination_grid = ILLUMINATION_GRID(illumination_number=Nil)
    angle_grid = illumination_grid.get_angle_grid()

    scat_tot = np.zeros((2 * Nil, 2 * lmax * (lmax + 2)), dtype=complex)
    inc_tot = np.zeros((2 * Nil, 2 * lmax * (lmax + 2)), dtype=complex)
    
    for iang in trange(2 * Nil):
        # TODO: PWE objects
        pol, kth, kph = angle_grid[iang]

        arbitrary_geom_pol_aniso(shape, params, n, rad, ar, Ngrid, ddaeps, Ntheta, Nphi,
                           kth, kph, nsub)

        data = adda_output.dipole_polarization_x if pol == 1 else adda_output.dipole_polarization_y
        pol_cart, r = far_field.parse_pol_file(data)
        pol_eff = far_field.get_total_polarization(pol_cart, grid.er, r, ks)
        pol_eff_sph = far_field.convert_cartesian_to_spherical(pol_eff, grid)
        Escat = far_field.calculate_far_field(pol_eff_sph, ks)
        pos = [0, 0, z]
        kth_arr = np.array([kth])
        azimuthal_angle = np.array([kph])[:,None]
        incf = pwe_to_swe_conversion(lmax, lmax, pos, pos, int(pol), np.sin(kth_arr), 
                                     np.cos(kth_arr), azimuthal_angle)
        
        scatf1 = calculate_scattered_field_coefficients(Escat, ks, lmax, grid)
        scat_tot[iang, :] = scatf1
        inc_tot[iang, :] = incf

    T = solve_t_matrix(scat_tot, inc_tot)
    return T

def t_matrix_factory(Nphi, Ntheta, Nil, ks, lmax, adda_callable, **kwargs):
    if "z" not in kwargs.keys():
        z = kwargs["rad"] * kwargs["ar"]
    else:
        z = kwargs["z"]
    grid = UniformIntegrationGrid(Nphi, Ntheta)
    illumination_grid = ILLUMINATION_GRID(illumination_number=Nil)
    angle_grid = illumination_grid.get_angle_grid()

    scat_tot = np.zeros((2 * Nil, 2 * lmax * (lmax + 2)), dtype=complex)
    inc_tot = np.zeros((2 * Nil, 2 * lmax * (lmax + 2)), dtype=complex)
    
    for iang in trange(2 * Nil):
        # TODO: PWE objects
        pol, kth, kph = angle_grid[iang]

        adda_output = adda_callable(**kwargs, kth=kth, kph=kph, Nphi=Nphi, Ntheta=Ntheta)

        data = adda_output.dipole_polarization_x if pol == 1 else adda_output.dipole_polarization_y
        pol_cart, r = far_field.parse_pol_file(data)
        pol_eff = far_field.get_total_polarization(pol_cart, grid.er, r, ks)
        pol_eff_sph = far_field.convert_cartesian_to_spherical(pol_eff, grid)
        Escat = far_field.calculate_far_field(pol_eff_sph, ks)
        pos = [0, 0, z]
        kth_arr = np.array([kth])
        azimuthal_angle = np.array([kph])[:,None]
        incf = pwe_to_swe_conversion(lmax, lmax, pos, pos, int(pol), np.sin(kth_arr), 
                                     np.cos(kth_arr), azimuthal_angle)
        
        scatf1 = calculate_scattered_field_coefficients(Escat, ks, lmax, grid)
        scat_tot[iang, :] = scatf1
        inc_tot[iang, :] = incf

    T = solve_t_matrix(scat_tot, inc_tot)
    return T

def t_matrix_from_adda_string(Nphi, Ntheta, Nil, lmax, adda_string, z, wavelength):
    ks = 2 * np.pi / wavelength
    T = t_matrix_factory(Nphi, Ntheta, Nil, ks, lmax, pol_from_adda_string, adda_string=adda_string, z=z)
    return T

def get_t_matrix(adda_string, max_order=2, num_theta=91, num_phi=181, num_illuminations=15, particle_z=0, wavelength=2 * np.pi):
    if particle_z == 0 and USE_SURF:
        prognosis = addatmatrix.wrapper.prognose_from_adda_string(adda_string)
        particle_z = addatmatrix.wrapper.get_particle_z(prognosis)
    if "lambda" in adda_string:
        wavelength_adda = float(adda_string.split("-lambda ")[1].split(" ")[0])
    else:
        wavelength_adda = 2 * np.pi
    t = t_matrix_from_adda_string(num_theta, num_phi, num_illuminations,
                                                      max_order, adda_string,
                                                      particle_z,
                                                      wavelength_adda)
    tmatrix_data, indices = addatmatrix.data.prepare_data_for_storage(t, max_order)
    metadata = addatmatrix.data.get_metadata(adda_string)
    # It is fine to leave most metadata from adda_string as arrays, but Tmatrix data format
    # requires float number for wavelength. Refractive index is transformed from array later on
    if "lambda" in metadata.keys():
        metadata["lambda"] = float(metadata["lambda"][0])
    else:
        metadata["lambda"] = wavelength
    if "m" not in metadata.keys():
        metadata["m"] = ['1.5', '0']
    metadata["num_illuminations"] = num_illuminations
    metadata["num_theta"] = num_theta
    metadata["num_phi"] = num_phi
    return tmatrix_data, metadata, indices

def calculate_orientation_averaged_scattering_cross_section(T):
    cscat_est = (np.abs(T)**2).sum() * 2 * np.pi
    return cscat_est

def estimate_orientation_averaged_scattering_cross_section(angle_grid, T, lmax, z=0):
    est_cscat_tot = []
    for ii in range(len(T)):
        pol, kth, kph = angle_grid[ii]
        pos = [0, 0, z]
        kth_arr = np.array([kth])
        azimuthal_angle = np.array([kph])[:,None]
        inc = pwe_to_swe_conversion(lmax, lmax, pos, pos, int(pol), np.sin(kth_arr), 
                                     np.cos(kth_arr), azimuthal_angle)

        est_cscat = T @ inc
        est_cscat = np.pi * np.sum(np.abs(est_cscat)**2)
        est_cscat_tot.append(est_cscat)

    cscat_est = np.mean(est_cscat_tot)
    return cscat_est

def calculate_orientation_averaged_extinction_cross_section(angle_grid, T, lmax):
    cext_est = - (np.trace(T.real)).sum() * 2 * np.pi
    return cext_est

def get_incident_field(polar_angle, azimuthal_angle, pol, lmax):
    z = 0
    pos = [0, 0, z]
    kth_arr = np.array([polar_angle])
    azimuthal_angle = np.array([azimuthal_angle])[:, None]
    incf = pwe_to_swe_conversion(lmax, lmax, pos, pos, int(pol), np.sin(kth_arr),
                                 np.cos(kth_arr), azimuthal_angle)
    return incf

def get_differential_scattering_cross_section(t_matrix, lmax, polar_angle, azimuthal_angle, vacuum_wavelength, theta_r):
    T_conv2 = addatmatrix.data.t_matrix_to_smuthi_convention(t_matrix, lmax)

    a_inc = get_incident_field(polar_angle=polar_angle,
                               azimuthal_angle=azimuthal_angle, pol=1, lmax=lmax)
    b = T_conv2 @ a_inc


    phi_r = np.linspace(0, 2*np.pi, 721) #azimuthal_angles
    phi, theta = np.meshgrid(phi_r, theta_r)

    lam = vacuum_wavelength
    k = 2 * np.pi / lam

    aa2 = far_field.azimuthal_average_far_field(b,theta,phi,k,lmax)
    return aa2