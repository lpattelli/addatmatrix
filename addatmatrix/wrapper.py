import subprocess
import numpy as np
import tempfile
import os
import addatmatrix
from dataclasses import dataclass, field
from typing import Optional

file_ext = "exe" if os.name=="nt" else "bin"
addapath=os.path.join(os.path.dirname(addatmatrix.__file__),f"adda.{file_ext}")
pol_to_coord = {1: "X", 0: "Y"}
USE_SURF = False

@dataclass
class AddaOutput:
    log: str
    cross_section_x: Optional[np.ndarray] = field(default=None)
    cross_section_y: Optional[np.ndarray] = field(default=None)
    dipole_polarization_x: Optional[np.ndarray] = field(default=None)
    dipole_polarization_y: Optional[np.ndarray] = field(default=None)
    geometry: Optional[str] = field(default=None)


    @classmethod
    def from_path(cls, path: str):
        output = {}
        if os.path.exists(os.path.join(path, "CrossSec-X")):
            output["cross_section_x"] = read_cross_sections(1, path)
        if os.path.exists(os.path.join(path, "CrossSec-Y")):
            output["cross_section_y"] = read_cross_sections(0, path)
        if os.path.exists(os.path.join(path, "DipPol-Y")):
            output["dipole_polarization_x"] = read_pol_file(1, path)
        if os.path.exists(os.path.join(path, "DipPol-Y")):
            output["dipole_polarization_y"] = read_pol_file(0, path)
        if os.path.exists(os.path.join(path, "log")):
            with open(os.path.join(path, "log"),"r") as f:
                output["log"] = f.readlines()
        num_geoms = len([path for path in os.listdir(path) if ".geom" in path])
        if num_geoms > 0:
            output["geometry"] = read_geom(path)

        return cls(**output)


def write_scattering_paramers(Ntheta,Nphi):
    a="""global_type=grid 
N=2
theta: 
type=range 
N="""+str(Ntheta)+""" 
min=0 
max=180
phi:
type=range
N="""+str(Nphi)+"""
min=0
max=360"""

    with open('scat_params.dat','w') as f:
        print(a,file=f)

def read_pol_file(pol: int, simulation_directory: str) -> np.array:
    pol_path = f'DipPol-{pol_to_coord[pol]}'
    total_path = os.path.join(simulation_directory, pol_path)
    data = np.loadtxt(total_path,skiprows=1)
    return data

def read_cross_sections(pol: int, simulation_directory: str) -> np.array:
    cross_path = f"CrossSec-{pol_to_coord[pol]}"
    total_path = os.path.join(simulation_directory, cross_path)
    with open(total_path,"r") as f:
        data = f.readlines()
    cross_sections = np.array([float(d.split()[-1]) for d in data])
    return cross_sections

def read_geom(simulation_directory: str) -> np.array:
    geom_path = [path for path in os.listdir(simulation_directory) if ".geom" in path][0]
    geom = np.loadtxt(os.path.join(simulation_directory, geom_path))
    return geom

def run_from_args(args):
    with tempfile.TemporaryDirectory() as temp_dir:
        subprocess.run([addapath] + list(filter(bool,args.split(" "))) + ["-dir", temp_dir], check=True,
                       stdout=subprocess.DEVNULL,
                       stderr=subprocess.STDOUT)
        adda_output = AddaOutput.from_path(temp_dir)
    return adda_output

def get_particle_z(adda_output):
    data = adda_output.log

    dipole_size = float([d for d in data if "Dipole size" in d][0].split()[2].split("x")[-1])
    number_of_dipoles_z = int([d for d in data if "box dimensions" in d][0].split()[2].split("x")[-1])
    particle_z = number_of_dipoles_z * dipole_size/2
    return particle_z

def prognose_from_adda_string(adda_string):
    args = adda_string +' -save_geom -prognosis'
    adda_output = run_from_args(args)
    return adda_output    

def pol_from_adda_string(adda_string, Ntheta=181,Nphi=181,kth=0,kph=0,ms=1+0j, z=0):
    nz=np.cos(kth)
    ny=np.sin(kth)*np.sin(kph)
    nx=np.sin(kth)*np.cos(kph)
    surf_string = ' -surf '+str(z)+' '+str(ms.real)+' '+str(ms.imag) if USE_SURF else ""
    args = adda_string + surf_string+' -prop '+str(nx)+' '+str(ny)+' '+str(nz)+' -save_geom -store_dip_pol -scat_matr none'
    adda_output = run_from_args(args)
    
    return adda_output

def ellipsoid_pol(x,m,yr=1,zr=1,grid=16,eps=3,Ntheta=181,Nphi=181,kth=0,kph=0,ms=1+0j):
    mr=m.real
    mi=m.imag
    msr=ms.real
    msi=ms.imag
    adda_string = '-shape ellipsoid '+str(yr)+' '+str(zr)+' -size '+str(2*x)+' -m '+str(mr)+' '+str(mi)
    adda_output = pol_from_adda_string(adda_string, Ntheta,Nphi,kth,kph,ms,zr)
    return adda_output

def ellipsoid_geom(x,yr,zr,grid=16):
    m=1.01
    eps=1
    ellipsoid_pol(x,m,yr,zr,grid,eps=3,Ntheta=11,Nphi=11,kth=0,kph=0,ms=1+0j)
    return None

def arbitrary_geom_pol(shape,params,m,x=1,zr=1,grid=16,eps=3,Ntheta=181,Nphi=181,kth=0,kph=0,ms=1+0j):
    mr=m.real
    mi=m.imag
    msr=ms.real
    msi=ms.imag
    params_string = " ".join([str(param) for param in params])
    adda_string = f'-shape {shape} '+params_string+' -size '+str(2*x)+' -m '+str(mr)+' '+str(mi) + f"-grid {grid}"
    adda_output = pol_from_adda_string(adda_string, Ntheta,Nphi,kth,kph,ms,zr*x)
    return adda_output

def arbitrary_geom_pol_aniso(shape,params,m,x=1,zr=1,grid=16,eps=3,Ntheta=181,Nphi=181,kth=0,kph=0,ms=1+0j):
    params_string = " ".join([str(param) for param in params])
    adda_string = f'-anisotr -shape {shape} '+params_string+' -size '+str(2*x)+' -m '+ m
    adda_output = pol_from_adda_string(adda_string, Ntheta,Nphi,kth,kph,ms,zr*x)
    return adda_output

def arbitrary_geom(shape,params,zr,grid=16):
    m=1.01
    eps=1
    arbitrary_geom_pol(shape,params,m,zr=zr,grid=grid,eps=eps,Ntheta=11,Nphi=11,kth=0,kph=0,ms=1+0j)
    return None


def geom_from_file(filepath, x, m, zr, grid=16,eps=3,Ntheta=181,Nphi=181,kth=0,kph=0,ms=1+0j):
    mr=m.real
    mi=m.imag
    msr=ms.real
    msi=ms.imag    
    write_scattering_paramers(Ntheta,Nphi)
    nz=np.cos(kth)
    ny=np.sin(kth)*np.sin(kph)
    nx=np.sin(kth)*np.cos(kph)
    run_from_args('-shape read '+str(filepath)+' -size '+str(2*x)+' -m '+str(mr)+' '+str(mi)+' -surf '+str(zr*x)+' '+str(msr)+' '+str(msi)+' -prop '+str(nx)+' '+str(ny)+' '+str(nz)+' -grid '+str(grid)+' -eps '+str(eps)+' -store_dip_pol')
    subprocess.run('cp run*/* .',shell=True)
    subprocess.run('rm -r run*',shell=True)

    return None
