from abc import abstractmethod
from typing import Optional

import numpy as np


class PlaneWave:
    def __init__(self, theta: float, phi: float, pol: int, wavenumber: Optional[float] = 1) -> None:
        self.theta = theta
        self.phi = phi
        self.pol = pol
        self.wavenumber = wavenumber

class IntegrationGrid:
    def __init__(self, phi, theta) -> None:
        self.phi = phi
        self.theta = theta
        P,T=np.meshgrid(phi,theta)
        er=np.array([np.sin(T)*np.cos(P),np.sin(T)*np.sin(P),np.cos(T)])        
        
        self.P = P
        self.T = T
        self.er = er

class UniformIntegrationGrid(IntegrationGrid):
    def __init__(self, n_phi: int, n_theta: int) -> None:
        phi = np.linspace(0, 2 * np.pi, n_phi)
        theta = np.linspace(0, np.pi, n_theta)
        super().__init__(phi, theta)

class IlluminationGrid:
    def __init__(self) -> None:
        pass

    @abstractmethod
    def get_angle_grid(self) -> np.array:
        raise NotImplementedError

class UniformIlluminationGrid(IlluminationGrid):
    def __init__(self, illumination_number: int, eps=1e-2) -> None:        
        self.angvec=np.linspace(0,np.pi-2*eps, illumination_number)+eps
        self.rng = np.random.RandomState(42)
    
    def get_angle_grid(self) -> np.array:
        angle_grid = []
        for ang in self.angvec:
            for pol in [0, 1]:
                kth=np.pi+ang
                kph=np.pi*self.rng.rand()
                angle_grid.append([pol, kth, kph])
        angle_grid = np.array(angle_grid)
        return angle_grid

class FibonacciIlluminationGrid(IlluminationGrid):
    def __init__(self, illumination_number: int) -> None:
        self.illumination_number = illumination_number
    
    def get_angle_grid(self) -> np.array:
        theta, phi = self.fibonacci_sphere_sampling(self.illumination_number)
        angle_grid = np.tile(np.array([theta * 0,theta, phi]).T,(2,1))
        angle_grid[self.illumination_number:, 0] = 1
        return angle_grid

    def fibonacci_sphere_sampling(self, n: int) -> tuple[np.ndarray]:
        gldr = np.pi * (np.sqrt(5)-1)
        y = np.linspace(-1, 1, 2*n+1)[1:-1:2] # avoid points in the xy plane
        r = np.sqrt(1 - np.power(y, 2))
        theta = gldr * np.arange(1, n+1)
        x = np.cos(theta) * r
        z = np.sin(theta) * r
        th = np.arctan2(np.hypot(x, y), z) + np.pi  # between pi and 2pi
        ph = np.arctan2(y, x)                       # between -pi and pi
        return th, ph

