import numpy as np
from scipy.special import sph_harm, factorial

from numba import complex64, complex128, float64, prange, jit, int32, int64, types

@jit(float64(int64), nopython=True, cache=True, nogil=True)
def jitted_prefactor(m):
    """Returns the prefactor :math:`\sqrt(\frac{(2*m+1)}{2(2m)!} (2m-1)!!`
    without using factorials nor bignum numbers, which makes it jittable.

    Args:
        m (int64): Argument (non-negative)

    Returns:
        :math:`\sqrt(\frac{(2*m+1)!!}{2(2m)!!}`
    """
    res = 1.
    for t in range(2,2*m+2,2):
        res += res/t # equivalent to res *= (t+1)/t, but retains all significant digits
    return (res/2)**0.5

@jit(nopython=True, cache=True, nogil=True)
def legendre_normalized_numbed(lval,mval, theta):
    ct = np.cos(theta)
    st = np.sin(theta)
    lmax = lval
    plm = np.zeros((lmax+1, lmax+1, *ct.shape), dtype=np.complex128)
    pilm = np.zeros((lmax+1, lmax+1, *ct.shape), dtype=np.complex128)
    taulm = np.zeros((lmax+1, lmax+1, *ct.shape), dtype=np.complex128)
    pprimel0 = np.zeros((lmax+1, *ct.shape), dtype=np.complex128)

    plm[0,0] = np.sqrt(2)/2
    plm[1, 0] = np.sqrt(3/2) * ct
    pprimel0[1] = np.sqrt(3) * plm[0, 0]
    taulm[0, 0] = -st * pprimel0[0]
    taulm[1, 0] = -st * pprimel0[1]

    for l in range(1, lmax):
        plm[l + 1, 0] = (1 / (l + 1) * np.sqrt((2 * l + 1) * (2 * l + 3)) * ct * plm[l, 0] -
                         l / (l + 1) * np.sqrt((2 * l + 3) / (2 * l - 1)) * plm[l-1, 0])
        pprimel0[l + 1] = ((l + 1) * np.sqrt((2 * (l + 1) + 1) / (2 * (l + 1) - 1)) * plm[l, 0] +
                           np.sqrt((2 * (l + 1) + 1) / (2 * (l + 1) - 1)) * ct * pprimel0[l])
        taulm[l + 1, 0] = -st * pprimel0[l + 1]

    for m in range(1, lmax + 1):
        prefactor = jitted_prefactor(m)
        plm[m, m] = prefactor * st**m
        pilm[m, m] = prefactor * st**(m - 1)
        taulm[m, m] = m * ct * pilm[m, m]
        for l in range(m, lmax):
            plm[l + 1, m] = (np.sqrt((2 * l + 1) * (2 * l + 3) / ((l + 1 - m) * (l + 1 + m))) * ct * plm[l, m] -
                             np.sqrt((2 * l + 3) * (l - m) * (l + m) / ((2 * l - 1) * (l + 1 - m) * (l + 1 + m))) *
                             plm[l - 1, m])
            pilm[l + 1, m] = (np.sqrt((2 * l + 1) * (2 * l + 3) / (l + 1 - m) / (l + 1 + m)) * ct * pilm[l, m] -
                              np.sqrt((2 * l + 3) * (l - m) * (l + m) / (2 * l - 1) / (l + 1 - m) / (l + 1 + m)) *
                              pilm[l - 1, m])
            taulm[l + 1, m] = ((l + 1) * ct * pilm[l + 1, m] -
                               (l + 1 + m) * np.sqrt((2 * (l + 1) + 1) * (l + 1 - m) / (2 * (l + 1) - 1) / (l + 1 + m))
                               * pilm[l, m])

    return plm[lval][mval], pilm[lval][mval], taulm[lval][mval]

def plm(l, m, x):
    ylm = sph_harm(m,l,0,x) * np.sqrt(2*np.pi) * (-1.)**(-m)
    return np.nan_to_num(ylm)

def pilm_taylor(l):
    fac= np.sqrt(0.5*(2*l+1) * factorial(l-1) / factorial(l+1))
    return 0.5 * fac * factorial(l+1) / (factorial(l-1))

def pilm(l, m, x):
    sin_x = np.sin(x)
    mask = np.abs(sin_x)<1e-5
    sin_x[mask] = 1 #x[mask]
    pi = plm(l, m, x) / sin_x
    if m == 0:
        pi = 0. * pi
    if m == 1 and l>=1:
        sign = 1 - 2 * (x[mask] == np.pi) * ((l-1) % 2)
        pi[mask] = pilm_taylor(l) * sign
    #if l == 1 and m==1:
    #    pi[pi == np.min(pi)] = np.max(pi)
    return pi

def pl(l, x):
    cos_x = np.cos(x) 
    if l == 0:
        return np.sqrt(2) / 2
    if l == 1:
        return np.sqrt(3/2) * cos_x
    else:
        n = l - 1
        return 1/(n+1) * np.sqrt((2*n+1)*(2*n+3)) * cos_x * pl(n, x) - n/(n+1) * np.sqrt((2*n+3) / (2*n-1)) * pl(n-1, x)

def derivative(l,x):
    if l == 0:
        return 0
    if l == 1:
        return np.sqrt(3/2)
    else:
        return l * np.sqrt((2*l+1) / (2*l-1)) * pl(l-1, x) + np.sqrt((2*l+1) / (2*l-1)) * np.cos(x) * derivative(l-1, x)

def taulm(l, m, x):
    cos_x = np.cos(x)
    sin_x = np.sin(x)
    sin_x[np.abs(sin_x)<1e-2] = 1
    if m < 0:
        tau = (l * cos_x * plm(l,m,x) - (l + m) * plm(l-1, m, x))/sin_x
    if m==0:
        tau = - np.sin(x) * derivative(l, x)
    if m>=1:
       fac = (l+m) * np.sqrt(((2*l+1)*(l-m))/((2*l-1)*(l+m)))
       tau = l * cos_x * pilm(l,m,x) - fac * pilm(l-1, m , x)
    return tau

def legendre_normalized(l, m, x):
    #p = plm(l, m, x)
    #pi = pilm(l, m, x)
    #tau = taulm(l, m, x)
    p, pi, tau = legendre_normalized_numbed(l, m, x)
    return p, pi, tau

def spherical_harmonic_m(theta,phi,l,m):
    _, pi, tau = legendre_normalized(l, np.abs(m), theta)

    al=1/np.sqrt(2*l*(l+1))
    M=al*np.array([1j*m*pi*np.exp(1j*m*phi),-tau*np.exp(1j*m*phi)])
    return M

def spherical_harmonic_n(theta,phi,l,m):
    _, pi, tau = legendre_normalized(l, np.abs(m), theta)

    al=1/np.sqrt(2*l*(l+1))
    M=al*np.array([tau*np.exp(1j*m*phi),1j*m*pi*np.exp(1j*m*phi)])
    return M


def transformation_coefficients_vwf(tau, l, m, pol, kp=None, kz=None, dagger=False):
    k = np.sqrt(kp**2 + kz**2)
    ct = kz / k
    st = kp / k
    theta = np.arctan2(st, ct)

    _, pilm, taulm = legendre_normalized(l, abs(m), theta)

    if tau == pol:
        sphfun = taulm
    else:
        sphfun = m * pilm

    if dagger:
        if pol == 0:
            prefac = -1 / (-1j) ** (l + 1) / np.sqrt(2 * l * (l + 1)) * (-1j)
        elif pol == 1:
            prefac = -1 / (-1j) ** (l + 1) / np.sqrt(2 * l * (l + 1)) * 1
        else:
            raise ValueError('pol must be 0 (TE) or 1 (TM)')

    B = prefac * sphfun
    return B


def pwe_to_swe_conversion(l_max, m_max, reference_point, pwe_reference_point, pwe_polarization, k_parallel, k_z, azimuthal_angle):
    kx = k_parallel * np.cos(azimuthal_angle)
    ky = k_parallel * np.sin(azimuthal_angle)
    kz = k_z[:,None]

    kvec = np.array([kx, ky, kz])
    rswe_mn_rpwe = np.array(reference_point) - np.array(pwe_reference_point)

    # phase factor for the translation of the reference point from rvec_iS to rvec_S
    ejkriSS = np.exp(1j * (kvec[0] * rswe_mn_rpwe[0] + kvec[1] * rswe_mn_rpwe[1] + kvec[2] * rswe_mn_rpwe[2]))

    # phase factor times pwe coefficients
    # TODO: fix pwe.coefficients
    pwe_coefficients = np.zeros(2, dtype=complex)
    pwe_coefficients[pwe_polarization] = 1. + 0j

    gejkriSS = pwe_coefficients[:,None,None] * ejkriSS[None, :, :]  # indices: pol, jk, ja

    coefficients = np.zeros(2*l_max*(l_max+2),dtype=complex)

    for m in range(-m_max, m_max + 1):
        emjma_geijkriSS = np.exp(-1j * m * np.squeeze(azimuthal_angle,0))[None, None, :] * gejkriSS
        for l in range(max(1, abs(m)), l_max + 1):
            for tau in range(2):
                ak_integrand = 0j
                for pol in range(2):
                    Bdag = transformation_coefficients_vwf(tau, l, m, pol=pol, kp=k_parallel, kz=k_z, dagger=True) # kzvec
                    ak_integrand += Bdag[:, None] * emjma_geijkriSS[pol, :, :]
                single_index = multi_to_single_index(tau, l, m, l_max, m_max)
                coefficients[single_index] = ak_integrand * 4
    
    return coefficients

def multi_to_single_index(tau, l, m, l_max, m_max):
    tau_blocksize = m_max * (m_max + 2) + (l_max - m_max) * (2 * m_max + 1)
    n = tau * tau_blocksize
    if (l - 1) <= m_max:
        n += (l - 1) * (l - 1 + 2)
    else:
        n += m_max * (m_max + 2) + (l - 1 - m_max) * (2 * m_max + 1)
    n += m + min(l, m_max)
    return n


def double_trapezoid_integral(integrand: np.array, grid):
    int2 = np.trapz(np.trapz(np.sum(integrand, axis=0)*np.sin(grid.T),
                             grid.phi), grid.theta)
    return int2
