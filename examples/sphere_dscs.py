import matplotlib.pyplot as plt
import numpy as np
import plotly.graph_objects as go

import addatmatrix.data as data
from addatmatrix.t_matrix import get_differential_scattering_cross_section, get_t_matrix
from addatmatrix.wrapper import prognose_from_adda_string
from addatmatrix.spatial import FibonacciIlluminationGrid


# Set the simulation parameters
adda_string = "-m 1.5 0 -lambda 400 -shape sphere -size 100"
num_illuminations = 64
max_order = 3
output_path = "tmatrix.h5"

# Run exemplary simulation and save the data
tmatrix_data, metadata, indices = get_t_matrix(adda_string, max_order, num_illuminations=num_illuminations)
data.write_t_matrix_data(output_path, tmatrix_data, indices, metadata)


# Read the data
data_dict = data.read_t_matrix_data(output_path)
T = data_dict['tmatrix_data']
lmax = np.max(data_dict['l'])

# Show geometry

prognosis = prognose_from_adda_string(adda_string)
geometry = prognosis.geometry

fig = plt.figure()
ax = fig.add_subplot(projection='3d')

ax.scatter(geometry[:,0], geometry[:,1], geometry[:,2])

# Show illumination grid
Nil = 64
Ngrid = 16

grid = FibonacciIlluminationGrid(Nil)
thetas, phis = grid.fibonacci_sphere_sampling(Nil)

fig = go.Figure(data = go.Cone(x=np.cos(phis) * np.sin(thetas),
                               y=np.sin(phis) * np.sin(thetas),
                               z=np.cos(thetas),
                               u=-np.cos(phis) * np.sin(thetas),
                               v=-np.sin(phis) * np.sin(thetas),
                               w=-np.cos(thetas),
                               colorscale=[[0,'blue'], [1,'blue']], showscale=False,
                               lighting_ambient=0.3, lighting_specular=0.9))
fig.show()

# Compare the output T-matrix to reference from smuthi
Tref = np.load("examples/data/t_ref.npy")

Tsm = data.t_matrix_to_smuthi_convention(T, lmax=3)

fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12,4))
fig.suptitle(f'absolute deviation, Nil={Nil}, Ngrid={Ngrid}')
real = ax1.imshow(np.abs(np.real(Tsm-Tref)), vmin=0, vmax=0.003)
ax1.set_title('abs(real($T - T_{tst}$))')
fig.colorbar(real, ax=ax1)
imag = ax2.imshow(np.abs(np.imag(Tsm-Tref)), vmin=0, vmax=0.003)
ax2.set_title('abs(imag($T - T_{tst}$))')
fig.colorbar(imag, ax=ax2)

# Calculate DSCS and plot the output result
dscs_ref = np.load("examples/data/dscs.npy")

theta_r = np.linspace(0, np.pi, 360)
polar_angle, azimuthal_angle, vacuum_wavelength = (np.pi*0.3,np.pi*0.1,400)
dscs = get_differential_scattering_cross_section(T, lmax, polar_angle, azimuthal_angle, vacuum_wavelength, theta_r)

plt.figure()
plt.plot(theta_r, dscs[0])
plt.plot(theta_r, dscs_ref[0])
plt.xlabel(r"$\theta$")
plt.ylabel(r"DSCS (averaged over $\phi$)")
plt.title("TE")

plt.figure()
plt.plot(theta_r, dscs[1])
plt.plot(theta_r, dscs_ref[1])
plt.xlabel(r"$\theta$")
plt.ylabel(r"DSCS (averaged over $\phi$)")
plt.legend(["ADDATmatrix", "Smuthi"])
plt.title("TM")
plt.show()

